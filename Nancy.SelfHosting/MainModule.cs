﻿using Nancy.SelfHosting.Model;
using Nancy.SelfHosting.Repository;
using System.Data.SQLite;
using Nancy.ModelBinding;
namespace Nancy.SelfHosting
{
    public class MainModule: NancyModule
    {
        private readonly IUserRepository _repo;

        public MainModule(IUserRepository repo) 
            :base("/api")
        {
            
            _repo = repo;
            // Get Single User
            Get["/{id:int}"] = x => { return FormatterExtensions.AsJson<User>(Response, _repo.Get(x.id)); };

            // Add new user
            Post["/"] = x =>
            {
                var data = repo.Create(this.Bind<User>());
                return data == true ? HttpStatusCode.OK : HttpStatusCode.InternalServerError;
            };

            Get["/delete/{id:int}"] = x => { return "Delete"; };

            Get["/"] = x => { return "User API"; };

        }
    }
}
