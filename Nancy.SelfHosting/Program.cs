﻿using Nancy.Hosting.Self;
using System;

namespace Nancy.SelfHosting
{
    class Program
    {
        static void Main(string[] args)
        {
            //Enable UrlReservations.CreateAutomatically on the HostConfiguration
            HostConfiguration hostConfigs = new HostConfiguration();
            hostConfigs.UrlReservations.CreateAutomatically = true;

            // initialize an instance of NancyHost (found in the Nancy.Hosting.Self package)
            var host = new NancyHost(new Uri("http://localhost:12345"), new AppBootstrapper(), hostConfigs);
            host.Start(); // start hosting

            Console.ReadKey();
            host.Stop();  // stop hosting
        }
    }
}
