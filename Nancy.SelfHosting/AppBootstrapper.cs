﻿using Nancy.Conventions;
using Nancy.TinyIoc;
using Nancy.SelfHosting.Model;
using Nancy.SelfHosting.Repository;
using System.Data.SQLite;

namespace Nancy.SelfHosting
{
    public class AppBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Scripts"));
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            var dbConnection = new SQLiteConnection("Data Source=Data.db;Version=3;");
            dbConnection.Open();
            IUserRepository repo = new UserRepository(dbConnection);
            container.Register(repo);
        }
    }
}
