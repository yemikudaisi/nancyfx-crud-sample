﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nancy.SelfHosting.Model
{
    public interface IUserRepository
    {        
        IEnumerable<User> GetAll();
        bool Create(User user);
        User Get(String id);
        bool Update(User user);
        void Delete(int id);
    }
}
