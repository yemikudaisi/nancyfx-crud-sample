﻿using Nancy.SelfHosting.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nancy.SelfHosting.Repository
{
    public class UserRepository : IUserRepository
    {

        SQLiteConnection _connection;

        public UserRepository(SQLiteConnection connection)
        {
            _connection = connection;
        }

        public bool Create(User user)
        {
            string sql = string.Format("insert into User (Firstname, LastName, Email) values ('{0}','{1}','{2}')",
                user.Firstname,
                user.Lastname,
                user.Email
                );
            var command = new SQLiteCommand(sql, _connection);
            return Convert.ToBoolean(command.ExecuteNonQuery());
        }

        public void Delete(int id)
        {
            var sql = string.Format("DELETE FROM User WHERE Id='{0}'", id);
            var command = new SQLiteCommand(sql, _connection);
            command.ExecuteNonQuery();
        }

        public User Get(string id)
        {
            string sql = "SELECT * FROM User WHERE Id='"+id+"'";
            var command = new SQLiteCommand(sql, _connection);
            var reader = command.ExecuteReader();
            if (reader.Read())
            {
                return UserFactory.BuildFromReader(reader);
            }else
            {
                return null;
            }
        }

        public IEnumerable<User> GetAll()
        {
            string sql = "SELECT * FROM User";
            var command = new SQLiteCommand(sql, _connection);
            var reader = command.ExecuteReader();
            List<User> result = new List<User>();
            while (reader.Read())
            {
                result.Add(UserFactory.BuildFromReader(reader));
            }
            return result;
        }

        public bool Update(User user)
        {
            string sql = string.Format("UPDATE User set Firstname = '{0}' ,LastName = '{1}', Email = '{2}' WHERE Id='{3}'",
                user.Firstname,
                user.Lastname,
                user.Email,
                user.Id
                );
            var command = new SQLiteCommand(sql, _connection);
            return Convert.ToBoolean(command.ExecuteNonQuery());
        }
    }
}
