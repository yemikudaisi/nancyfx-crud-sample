﻿using Nancy.SelfHosting.Model;
using System;
using System.Data.SQLite;

namespace Nancy.SelfHosting.Repository
{
    public class UserFactory
    {
        public static User BuildFromReader(SQLiteDataReader reader)
        {
            var user = new User();
            user.Id = Convert.ToInt32(reader["Id"]);
            user.Firstname = (String)reader["Firstname"];
            user.Lastname = (String)reader["Lastname"];
            user.Email = (String)reader["Email"];
            return user;
        }
    }
}
